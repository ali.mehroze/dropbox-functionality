from google.appengine.ext import ndb

class BlobCollection(ndb.Model):
    filenames = ndb.StringProperty()
    blobs = ndb.BlobKeyProperty()


class Directories(ndb.Model):
    name = ndb.StringProperty()
    user_id = ndb.StringProperty()
    subdirectories = ndb.KeyProperty(kind = 'Directories', repeated = True)
    parentdirectory = ndb.KeyProperty(kind= 'Directories')
    files = ndb.KeyProperty(kind = 'BlobCollection', repeated = True)
