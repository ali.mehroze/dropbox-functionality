import webapp2
import jinja2
from google.appengine.ext import ndb
from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
import webapp2
import jinja2
from google.appengine.api import users
import os

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class DownloadHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self):
        file_obj_id= self.request.get('file_obj')
        collection = blobstore.get(file_obj_id)
        # file_obj_key = ndb.BlobKey('BlobCollection',file_obj_key.id())
        # file_obj = file_obj_key.get()
        self.send_blob(collection)

        # collection_key = ndb.Key('BlobCollection', 1)


        # self.send_blob(collection.blobs[index])
        # index = int(self.request.get('index'))
        # collection_key = ndb.Key('BlobCollection', file_obj_id)
        # collection = collection_key.get()
        # self.send_blob(collection.blobs[index])
        # 
        # template_values = {"template_debug":"collection", "file_obj": collection}
        # template = JINJA_ENVIRONMENT.get_template('debugging.html')
        # self.response.write(template.render(template_values))
