from google.appengine.ext import blobstore
from google.appengine.ext import ndb
from google.appengine.ext.webapp import blobstore_handlers
from directories import Directories
from directories import BlobCollection
import webapp2
import jinja2
from google.appengine.api import users
import os

JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):

    def post(self):
        if self.request.get('myButton')!='Delete':
            upload = self.get_uploads()[0]
            blobinfo = blobstore.BlobInfo(upload.key())
            filename = blobinfo.filename
            collection = BlobCollection(filenames=filename, blobs=upload.key())

            collection_key = collection.put()
            user_id = self.request.get('user_id')
            directory_name = self.request.get('directory_name')
            temp_dir = Directories.query(Directories.user_id==user_id, Directories.name==directory_name).fetch()[0]


            temp_dir.files.append(collection_key)
            temp_dir.put()
            self.redirect('/')

        elif self.request.get('myButton')=='Delete':
            user_id = self.request.get('usr_id')
            directory_name = self.request.get('dir_name')
            file = self.request.get('files')

            # temp_dir = Directories.query(Directories.user_id==user_id, Directories.name==directory_name).fetch()[0]
            # collection_key = ndb.Key('BlobCollection',file)
            # collection = collection_key.get()
            # temp_dir.files.remove(collection_key)
            # temp_dir.put()
            current_directory = Directories.query(Directories.user_id==user_id, Directories.name==directory_name).fetch()
            if current_directory:
                current_directory = current_directory[0]

            temp_file = BlobCollection.query(BlobCollection.filenames==file).fetch()

            if temp_file:


                temp_file = temp_file[0]
                current_directory.files.remove(temp_file.key)
                current_directory.put()
                temp_file.key.delete()


            self.redirect('/')
