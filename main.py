import webapp2
import jinja2
from google.appengine.api import users
import os
from google.appengine.ext import ndb
from myuser import MyUser2
from directories import Directories
from directories import BlobCollection
from google.appengine.ext import blobstore
from uploadhandler import UploadHandler
from downloadhandler import DownloadHandler





JINJA_ENVIRONMENT = jinja2.Environment(
loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
extensions=['jinja2.ext.autoescape'],
autoescape=True
)
JINJA_ENVIRONMENT.globals['uri_for'] = webapp2.uri_for

class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/html'
		# URL that will contain a login or logout link
		url = ''
		url_string = ''
		welcome_message = 'Welcome back'
		# pull the current user from the request
		user = users.get_current_user()

		template_debug = ''
		display_string=''
		myuser=''
		directories = ''
		current_dir_name = ''
		current_directory = ''
		files = ''


		if user:
			url= users.create_logout_url(self.request.uri)

			url_string = 'logout'

			myuser_key = ndb.Key('MyUser2',user.user_id())
			myuser = myuser_key.get()


			# switch = self.request.get('button')
			if myuser == None:
				welcome_message = 'Welcome to DropBox'

				myuser = MyUser2(id = user.user_id())
				myuser_key = myuser.put()
				root_dir = Directories(user_id=user.user_id(), name = '/')
				root_dir.put()


			if self.request.get('current_directory_name'):
				current_dir_name = self.request.get('current_directory_name')

			else:
				current_dir_name = '/'


			current_directory = Directories.query(Directories.user_id==user.user_id(), Directories.name==current_dir_name).fetch()
			if current_directory:
				current_directory = current_directory[0]
				files = current_directory.files




			switch = self.request.get('button')
			if switch =='Add':

				dir_name = self.request.get('dir_name')
				temp_dir_key = ndb.Key('Directories',current_directory.key.id())
				temp_dir = temp_dir_key.get()

				new_dir = Directories.query(Directories.user_id==user.user_id(), Directories.name==dir_name).fetch()

				if len(new_dir)>0:
					self.redirect('/')

				else :
					new_dir = Directories(name = dir_name, user_id=user.user_id(), parentdirectory= current_directory.key)
					new_dir_key = new_dir.put()
					temp_dir.subdirectories.append(new_dir_key)
					temp_dir.put()
				# self.redirect('/')

			elif switch == 'Delete':
				dir_name = self.request.get('del_dir_name')
				temp_name = self.request.get('key_name')

				current_directory = Directories.query(Directories.user_id==user.user_id(), Directories.name==temp_name).fetch()
				if current_directory:
					current_directory = current_directory[0]

				new_dir = Directories.query(Directories.user_id==user.user_id(), Directories.name==dir_name).fetch()

				if new_dir:
					temp_dir_key = ndb.Key('Directories',current_directory.key.id())
					temp_dir = temp_dir_key.get()

					new_dir = new_dir[0]
					temp_dir.subdirectories.remove(new_dir.key)
					temp_dir.put()
					new_dir.key.delete()

				else:
					self.redirect('/')

			elif switch == 'View':
				temp_name = self.request.get('key_name')
				temp_parent = self.request.get('key_name')
				current_directory = Directories.query(Directories.user_id==user.user_id(), Directories.name==temp_name).fetch()
				if current_directory:
					current_directory = current_directory[0]










		else:
			url = users.create_login_url(self.request.uri)
			url_string = 'Login'

		template_values ={
			'url':url,
			'url_string': url_string,
			'display_string': display_string,
			'user': myuser,
			'welcome_message':welcome_message,
			'directories': current_directory,
			'template_debug':template_debug,
			'collection' : files,
			'upload_url' : blobstore.create_upload_url('/upload')
		}

		template = JINJA_ENVIRONMENT.get_template('main.html')
		self.response.write(template.render(template_values))





app = webapp2.WSGIApplication([
webapp2.Route(r'/', handler=MainPage, name = 'mainpage'),
webapp2.Route(r'/upload', handler=UploadHandler),
webapp2.Route(r'/download', handler=DownloadHandler)
],debug=True)
